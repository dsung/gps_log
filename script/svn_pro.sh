#!/bin/sh
# set property to text/html
if [ "$1" == "" ]; then
  echo "usage: $0 fn.html"
  exit 0
fi

svn propset svn:mime-type text/html $1
